(ns Player
  (:gen-class))

;; Auto-generated code below aims at helping you parse
;; the standard input according to the problem statement.

(defn -main [& args]
  (while true
    (let [space-x (read)
          space-y (read)
          mountain-h (repeatedly 8 read)
          ;; mountain-h (into (sorted-map-by (fn [key1 key2]
          ;;                                   (compare [(get results key2) key2]
          ;;                                            [(get results key1) key1])))
          ;;                  (map-indexed vector (repeatedly 8 read)))
          max-mountain (apply max mountain-h)]
      ;; mountain-h: represents the height of 8 mountains, from 9 to 0.
      ;;   Mountain heights are provided from left to right.

      ;; either:
      ;;   FIRE (ship is firing its phase cannons) or
      ;;   HOLD (ship is not firing).
      (doseq [h mountain-h]
        (binding [*out* *err*]
          (println "deciding with" h "in" mountain-h)
          (println "x,y:" [space-x space-y]))
        (if (= h max-mountain)
          (println "FIRE")
          (println "HOLD"))))))
