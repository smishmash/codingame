(ns Player
  (:gen-class))

;; Auto-generated code below aims at helping you parse
;; the standard input according to the problem statement.
;; ---
;; Hint: You can use the debug stream to print initial-thor-x and initial-thor-y,
;; if Thor seems not follow your orders.

(def move-to-compass
  {[0 -1] "N"
   [0 1] "S"
   [1 0] "E"
   [-1 0] "W"
   [1 -1] "NE"
   [-1 -1] "NW"
   [1 1] "SE"
   [-1 1] "SW"})

(defn -main [& args]
  (let [light-x (read)
        light-y (read)
        initial-thor-x (read)
        initial-thor-y (read)]
    ;; light-x: the X position of the light of power
    ;; light-y: the Y position of the light of power
    ;; initial-thor-x: Thor's starting X position
    ;; initial-thor-y: Thor's starting Y position
    (loop [thor-x initial-thor-x
           thor-y initial-thor-y]
      (let [remainingTurns (read)
            x-move (Integer/signum (- light-x thor-x))
            y-move (Integer/signum (- light-y thor-y))]
        ;; remainingTurns: The remaining amount of turns Thor can move. Do not remove this line.

        ;; (binding [*out* *err*]
        ;;   (println "Debug messages..."))

        ;; A single line providing the move to be made: N NE E SE S SW W or NW
        (println (move-to-compass [x-move y-move]))
        (recur (+ thor-x x-move)
               (+ thor-y y-move))))))
