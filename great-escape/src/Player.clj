(ns Player
  (:gen-class))

(def right "RIGHT")
(def left "LEFT")
(def up "UP")
(def down "DOWN")

(defmacro debug
  [form]
  `(binding [*out* *err*]
     (prn '~form :=> ~form)))

(defn read-game-configuration
  []
  ;; w: width of the board
  ;; h: height of the board
  ;; playerCount: number of players (2 or 3)
  ;; myId: id of my player (0 = 1st player, 1 = 2nd player, ...)
  (into {} (map #(vector % (read)) [:width :height :player-count :my-id])))

(defn read-players
  [player-count]
  ;; x: x-coordinate of the player
  ;; y: y-coordinate of the player
  ;; walls-left: number of walls available for the player
  (for [index (range player-count)]
    (into {:index index} (map #(vector % (read)) [:x :y :walls-left]))))

(defn read-walls
  [wall-count]
  (repeatedly wall-count
              ;; x: x-coordinate of the wall
              ;; y: y-coordinate of the wall
              ;; orientation: wall orientation ('H' or 'V')
              (fn [] (into {} (map #(vector % (read)) [:x :y :orientation])))))

(defn select-action
  [game players walls]
  (let [{:keys [my-id]} game]
    (nth [right left up] my-id)))

(defn -main [& args]
  (let [game (read-game-configuration)]
    (loop []
      (let [players (read-players (:player-count game))
            walls (read-walls (read))
            ;; action: LEFT, RIGHT, UP, DOWN or "put-x put-y put-orientation" to place a wall
            action (select-action game players walls)]
        (println action)
        (recur)))))
