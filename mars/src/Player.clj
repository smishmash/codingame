(ns Player
  (:gen-class))

;; Auto-generated code below aims at helping you parse
;; the standard input according to the problem statement.

(defn -main [& args]
  (let [surf-pts (read)]
    ;; surf-pts: the number of points used to draw the surface of Mars.
    (loop [i surf-pts]
      (when (> i 0)
        (let [land-x (read)
              land-y (read)]
          ;; land-x: X coordinate of a surface point. (0 to 6999)
          ;; land-y: Y coordinate of a surface point. By linking all
          ;;   the points together in a sequential fashion, you form
          ;;   the surface of Mars.
          (recur (dec i)))))
    (while true
      (let [xpos (read)
            ypos (read)
            h-speed (read)
            v-speed (read)
            fuel (read)
            rotate (read)
            power (read)]
        ;; h-speed: the horizontal speed (in m/s), can be negative.
        ;; v-speed: the vertical speed (in m/s), can be negative.
        ;; fuel: the quantity of remaining fuel in liters.
        ;; rotate: the rotation angle in degrees (-90 to 90).
        ;; power: the thrust power (0 to 4).

        ;; (binding [*out* *err*]
        ;;   (println "Debug messages..."))

        ;; 2 integers: rotate power.
        ;;   rotate is the desired rotation angle (should be 0 for level 1),
        ;;   power is the desired thrust power (0 to 4).
        (if (> v-speed -30)
          (println "0 1")
          (println "0 4"))))))
