(ns Player
  (:gen-class))

;;; Auto-generated code below aims at helping you parse
;;; the standard input according to the problem statement.

(def act-speed "SPEED")
(def act-slow "SLOW")
(def act-jump "JUMP")
(def act-wait "WAIT")

(defn go-if-stopped
  [{:keys [speed]}]
  (when (= speed 0) act-speed))

(defn accelerate-to-jump-speed
  [{:keys [speed road x platform]}]
  (when (< x road)
    (let [rest-road (- road x)
          largest-platform-multiple (int (/ rest-road platform))
          platform-remainder (rem rest-road platform)]
      (cond
        (and (= speed platform) (= platform-remainder 0)) act-wait
        ()
        )))
  )

(def all-actions [go-if-stopped accelerate-to-jump-speed])

(defn -main [& args]
  (let [road (read) gap (read) platform (read)]
    ;; road: the length of the road before the gap.
    ;; gap: the length of the gap.
    ;; platform: the length of the landing platform.
    (while true
      ;; speed: the motorbike's speed.
      ;; x: the position on the road of the motorbike.
      (let [speed (read)
            x (read)
            state (:road road :gap gap :platform platform :speed speed :x x)]
        ;; (binding [*out* *err*]
        ;;   (println "Debug messages..."))
        ;; A single line containing one of 4 keywords: SPEED, SLOW, JUMP, WAIT.
        (println (or (some #(% state) all-actions) act-wait))))))
