(ns Player
  (:gen-class))

;; Auto-generated code below aims at helping you parse
;; the standard input according to the problem statement.

(def max-x 35)
(def max-y 20)

(def max-tries 10)

(defmacro debug
  [form]
  `(binding [*out* *err*]
     (prn '~form :=> ~form)))

(defmacro trace
  [prompt form]
  `(binding [*out* *err*]
     (let [result# ~form]
       (println ~prompt (prn-str result#))
       result#)))

(defn read-me
  []
  ;; round: current round
  ;; x: Your x position
  ;; y: Your y position
  ;; backInTimeLeft: Remaining back in time
  {:round (read) :x (read) :y (read) :back-in-time-left (read)})

(defn read-opponents
  [opp-count]
  ;; opponent-x: X position of the opponent
  ;; opponent-y: Y position of the opponent
  ;; opponentBackInTimeLeft: Remaining back in time of the opponent
  (doall (repeatedly opp-count (fn [] {:x (read) :y (read) :back-in-time-left (read)}))))

(defn read-arena
  []
  ;; line: One line of the map ('.' = free, '0' = you, otherwise the id of the opponent)
  (read-line)
  (doall (into [] (repeatedly max-y read-line))))

;;; Grid calcs

(defn same-location?
  [l1 l2]
  (and (= (:x l1) (:x l2))
       (= (:y l1) (:y l2))))

(defn distance ^long
  ([{^long x1 :x ^long y1 :y} {^long x2 :x ^long y2 :y}]
   (distance x1 y1 x2 y2))
  ([^long x1 ^long y1 ^long x2 ^long y2]
   (let [dx (- x1 x2)
         dy (- y1 y2)]
     (long (Math/sqrt (+ (* dx dx) (* dy dy)))))))

;;; Plan primitives

(defmulti apply-plan
  (fn [plan arena me opponents] (:phase (first plan))))

(defmulti terminate?
  (fn [plan arena me opponents] (:phase (first plan))))

;;; Breadth First Search

(defprotocol TreeNode
  ;; Get children, returns collection of TreeNode
  (node-children [this])
  ;; Current position in tree
  (node-depth [this])
  ;; Data associated with tree node
  (node-data [this]))

(defrecord GridTree [arena x y depth]
  TreeNode
  (node-children [this]
    (letfn [(new-tree [nx ny]
              (when (and (< -1 nx max-x)
                         (< -1 ny max-y))
                (->GridTree arena nx ny (inc depth))))]
      (let [x-1 (dec x)
            x+1 (inc x)
            y-1 (dec y)
            y+1 (inc y)]
        (filter some? [(new-tree x-1 y-1) (new-tree x y-1) (new-tree x+1 y-1)
                       (new-tree x-1 y)                    (new-tree x+1 y)
                       (new-tree x-1 y+1) (new-tree x y-1) (new-tree x+1 y+1)]))))
  (node-depth [this]
    depth)
  (node-data [this]
    {:x x :y y}))

(defn breadth-first-search [tree leaf-node? max-depth truncate-path?]
  ((fn step [queue seen?]
     (lazy-seq
      (when (seq queue)
        (let [path (peek queue)
              node (last path)]
          (cond
            (seen? (node-data node)) (step (pop queue) seen?)
            (>= (node-depth node) max-depth) (step (pop queue) (conj seen? (node-data node)))
            (truncate-path? path) (step (pop queue) (conj seen? (node-data node)))
            (leaf-node? node) (cons path (step (pop queue) (conj seen? (node-data node))))
            :else (step (into (pop queue) (map (partial conj path) (node-children node))) (conj seen? (node-data node))))))))
   (conj clojure.lang.PersistentQueue/EMPTY [tree]) #{}))

;;; Random selection procedure

(defn leg-completed?
  [leg arena me opponents]
  (same-location? leg me))

(defn generate-within-distance
  [min-distance max-distance]
  (if (= min-distance max-distance)
    min-distance
    (let [d (rand-int (- max-distance min-distance))
          sign (if (zero? (rand-int 2)) 1 -1)]
      (* sign (+ d min-distance)))))

(defn select-randomly
  [selector arena me opponents distance]
  (loop [min-distance distance max-distance distance iter 0]
    (when (< iter max-tries)
      (let [[dx dy] (repeatedly 2 #(generate-within-distance min-distance max-distance))
            {mx :x my :y} me
            cx (+ mx dx)
            cy (+ my dy)
            x (min (max cx 0) max-x)
            y (min (max cy 0) max-y)
            target (get-in arena [y x])]
        (if (selector target)
          {:x x :y y}
          (recur (max (dec min-distance) 2) (min (inc max-distance) 25) (inc iter)))))))

(defn select-optimal-for-opponent
  [me opponent]
  (let [{mx :x my :y} me
        {ox :x oy :y} opponent]
    {:x (int (* 0.49 (+ mx ox)))
     :y (int (* 0.49 (+ my oy)))}))

(defn relative-quadrant
  [center point]
  (let [{cx :x cy :y} center
        {px :x py :y} point]
    [(Integer/signum (- px cx))
     (Integer/signum (- py cy))]))

(defn select-closest
  [center points]
  (apply min-key (partial distance center) points))

(defn select-considering-opponents
  [arena me opponents]
  (map (partial select-closest me)
       (vals (group-by (partial relative-quadrant me)
                       (map (partial select-optimal-for-opponent me) opponents)))))

(def all-corners
  [{:x 0 :y 0}
   {:x 0 :y (dec max-y)}
   {:x (dec max-x) :y 0}
   {:x (dec max-x) :y (dec max-y)}])

;;; BFS Selection Procedure

(defn select-closest-empty-square
  [arena me opponents]
  (let [bfs (breadth-first-search (->GridTree arena (:x me) (:y me) 0)
                                  (fn [node]
                                    (let [{x :x y :y} (node-data node)]
                                      (and (> (node-depth node) 0)
                                           (= (get-in arena [y x]) \.))))
                                  max-y
                                  (constantly false))
        best-path (first bfs)]
    (when best-path
      (last best-path))))

;;; Quick Completion Plan

;;; Can we draw a line from current position to another nearby
;;; position of mine that's separated by spaces?

(def quick-completion-padding 3)
(def quick-completion-max-radius (int (/ max-y 2)))

(derive ::quick-completion ::box)

(defn quick-completion-plan
  [arena me opponents]
  (let [plan-leaf-node? (fn [node]
                          (let [{x :x y :y} (node-data node)]
                            (and (>= (node-depth node) quick-completion-padding)
                                 (= (get-in arena [y x]) \0))))
        path-doubles-back? (fn [path]
                             (and (> (count path) 2)
                                  (let [pairs (partition-all 2 1 path)
                                        x-sigs (into #{} (map (fn [[n1 n2]]
                                                                (when n2
                                                                  (Integer/signum (- (:x n1) (:x n2)))))
                                                              pairs))
                                        y-sigs (into #{} (map (fn [[n1 n2]]
                                                                (when n2
                                                                  (Integer/signum (- (:y n1) (:y n2)))))
                                                              pairs))]
                                    (or (and (contains? x-sigs 1) (contains? x-sigs -1))
                                        (and (contains? y-sigs 1) (contains? y-sigs -1))))))
        term-plan-path? (fn [path]
                          (or (let [node (last path)
                                    {x :x y :y} (node-data node)]
                                (and (> (node-depth node) 0)
                                     (not= (get-in arena [y x]) \.)))
                              (path-doubles-back? path)))
        bfs (breadth-first-search (->GridTree arena (:x me) (:y me) 0)
                                  plan-leaf-node?
                                  quick-completion-max-radius
                                  term-plan-path?)
        best-path (first bfs)]
    (when best-path
      (map #(assoc {:phase ::quick-completion}
                   :x (:x %) :y (:y %))
           best-path))))

(defmethod terminate? ::quick-completion
  [plan arena me opponents]
  false)

;;; Box content frequency

(defn coord-range
  [x1 x2]
  (if (< x1 x2)
    (range x1 x2)
    (range (dec x1) (dec x2) -1)))

(defn content-frequency-on-x-leg
  [arena y x1 x2]
  (frequencies (drop (min x1 x2) (take (max x1 x2) (nth arena y)))))

(defn content-frequency-on-y-leg
  [arena x y1 y2]
  (frequencies (drop (min y1 y2) (take (max y1 y2) (mapv #(nth % x) arena)))))

(defn content-frequency-on-leg
  [arena [leg-start leg-end]]
  (let [{x1 :x y1 :y} leg-start
        {x2 :x y2 :y} leg-end]
    (cond
      (and (= x1 x2) (= y1 y2)) {}
      (= x1 x2) (content-frequency-on-y-leg arena x1 y1 y2)
      :else (content-frequency-on-x-leg arena y1 x1 x2))))

(defn content-frequency-along-box
  [plan arena me]
  (apply merge-with
         +
         (map (partial content-frequency-on-leg arena)
              (mapv vector (concat [me] (butlast plan)) plan))))

(defn content-frequency-in-box
  [plan arena me]
  (let [[{x1 :x y1 :y} _ {x2 :x y2 :y}] plan]
    (when (and x2 y2)
      (apply merge-with
             +
             (map #(content-frequency-on-x-leg arena % x1 x2)
                  (range (min y1 y2) (max y1 y2)))))))

;;; Box construction plan

(declare migration-plan)

(defn ->plan
  [arena me target]
  (let [{x1 :x y1 :y} me
        {x2 :x y2 :y} target
        xsig (Integer/signum (- x2 x1))
        plan (if (= \. (get-in arena [y1 (+ x1 xsig)]))
               [{:x x2 :y y1 :phase ::box}
                {:x x2 :y y2 :phase ::box}
                {:x x1 :y y2 :phase ::box}
                {:x x1 :y y1 :phase ::box}]
               [{:x x1 :y y2 :phase ::box}
                {:x x2 :y y2 :phase ::box}
                {:x x2 :y y1 :phase ::box}
                {:x x1 :y y1 :phase ::box}])]
    {:source me :destination target :steps plan}))

(defn scored-plan
  [arena me opponents plan]
  (let [cfreq (content-frequency-in-box (:steps plan) arena me)
        {{^long sx :x ^long sy :y} :source {^long dx :x ^long dy :y} :destination} plan
        lx (inc (Math/abs (- sx dx)))
        ly (inc (Math/abs (- sy dy)))
        aspect-ratio (/ (min lx ly) (max lx ly))
        size-threshold (- (/ (* max-x max-y)
                             (inc (count opponents)))
                          (Math/sqrt (:round me)))
        score (if (or (some #(> (get cfreq % 0) 0) [\1 \2 \3])
                      (> (* lx ly) size-threshold))
                0.0
                (* (/ (get cfreq \. 0) (inc (get cfreq \0 0)))
                   aspect-ratio
                   aspect-ratio))]
    (assoc plan :score score)))

(defn plan-box
  [arena me opponents]
  (let [select-best-plan #(let [plans (map (comp (partial scored-plan arena me opponents)
                                                 (partial ->plan arena me))
                                           %)]
                            (when-not (empty? plans)
                              (apply max-key :score plans)))
        best-by-opponents (select-best-plan (select-considering-opponents arena me opponents))
        best-by-corners (select-best-plan all-corners)]
    (cond
      (>= (* 2 (:score best-by-opponents 0)) (max 8 (:score best-by-corners 0))) (:steps best-by-opponents)
      (>= (:score best-by-corners 0) 4) (:steps best-by-corners)
      :else (let [best-random (select-best-plan
                               (filter (complement nil?)
                                       (repeatedly max-tries
                                                   #(select-randomly #{\.} arena me opponents 10))))]
              (when (>= (:score best-random 0) (- 20 (int (Math/pow (:round me) 0.4))))
                (:steps best-random))))))

(defmethod apply-plan ::box
  [plan arena me opponents]
  plan)

(defmethod terminate? ::box
  [plan arena me opponents]
  (or (some (fn [o]
              (and (= (:x me) (:x o))
                   (= (:y me) (:y o))))
            opponents)
      (let [cfreq (content-frequency-along-box plan arena me)]
        (or (= (get cfreq \. 0) 0)
            (some #(> (get cfreq % 0) 0) [\1 \2 \3])))))

;;; 2d int array

(defrecord LongArray2d [width height data])

(defn make-2d
  [width height]
  (->LongArray2d width height (long-array (* width height) 0)))

(defn get-2d
  [{:keys [width ^longs data]} x y]
  (aget data (+ (* width y) x)))

(defn set-2d
  [{:keys [width ^longs data]} x y ^long val]
  (aset data (+ (* width y) x) val))

;;; Migration Plan

(def no-color (Object.))
(def no-content (Object.))

(defprotocol FloodFillPoint
  (get-content [this])
  (get-color [this])
  (set-color [this color])
  (move-west [this])
  (move-east [this])
  (move-north [this])
  (move-south [this]))

(declare no-point)

(defrecord ArenaPoint [x y arena arena-colors]
  FloodFillPoint
  (get-content [this]
    (or (get-in arena [y x])
        no-content))
  (get-color [this]
    (let [result (get-2d arena-colors x y)]
      (if (= result 0)
        no-color
        result)))
  (set-color [this color]
    (set-2d arena-colors x y color))
  (move-west [this]
    (if (> x 0)
      (->ArenaPoint (dec x) y arena arena-colors)
      no-point))
  (move-east [this]
    (if (< x (dec max-x))
      (->ArenaPoint (inc x) y arena arena-colors)
      no-point))
  (move-north [this]
    (if (> y 0)
      (->ArenaPoint x (dec y) arena arena-colors)
      no-point))
  (move-south [this]
    (if (< y (dec max-y))
      (->ArenaPoint x (inc y) arena arena-colors)
      no-point)))

(def no-point
  (reify FloodFillPoint
    (get-content [this]
      no-content)
    (get-color [this]
      no-color)
    (move-west [this]
      no-point)
    (move-east [this]
      no-point)
    (move-north [this]
      no-point)
    (move-south [this]
      no-point)))

(defn node-color-replaceable?
  [n target-content replacement-color]
  (and (not= (get-color n) replacement-color)
       (let [content (get-content n)]
         (and (= content target-content)
              (not= content no-content)))))

(defn collect-node-line
  [node target-content replacement-color]
  (let [result (list)
        result (cons node result)
        result (loop [w (move-west node) west-nodes result]
                 (if (node-color-replaceable? w target-content replacement-color)
                   (recur (move-west w) (cons w west-nodes))
                   west-nodes))
        result (loop [e (move-east node) east-nodes result]
                 (if (node-color-replaceable? e target-content replacement-color)
                   (recur (move-east e) (cons e east-nodes))
                   east-nodes))]
    result))

(defn flood-fill
  [node target-content ^long replacement-color]
  (if (= (get-content node) target-content)
    (loop [q (conj (clojure.lang.PersistentQueue/EMPTY) node)]
      (when-not (empty? q)
        (let [n (peek q)]
          (assert (< (count q) (* max-x max-y)))
          (recur
           (loop [line (collect-node-line n target-content replacement-color) q (pop q)]
             (if (empty? line)
               q
               (let [fln (first line)
                     north (move-north fln)
                     south (move-south fln)]
                 (set-color fln replacement-color)
                 (let [q (if (node-color-replaceable? north target-content replacement-color)
                           (do
                             (set-color north replacement-color)
                             (conj q north))
                           q)
                       q (if (node-color-replaceable? south target-content replacement-color)
                           (do
                             (set-color south replacement-color)
                             (conj q south))
                           q)]
                   (recur (rest line) q)))))))))))

(defn flood-fill-arena
  [arena]
  (let [coloration (make-2d max-x max-y)
        color (atom 0)]
    (doseq [x (range max-x)
            y (range max-y)]
      (when (= (get-2d coloration x y) 0)
        (flood-fill (->ArenaPoint x y arena coloration)
                    (get-in arena [y x])
                    (int (swap! color inc)))))
    [coloration (inc @color)]))

(defn update-region-summary
  [arena {snum-points :num-points smin-x :min-x smax-x :max-x smin-y :min-y smax-y :max-y
          ^long sdistance :distance scontent :content :as summary}
   x y ndistance]
  (cond-> (assoc! summary
                  :num-points (inc snum-points)
                  :min-x (min x smin-x)
                  :min-y (min y smin-y)
                  :max-x (max x smax-x)
                  :max-y (max y smax-y))
    (and (< ndistance sdistance) (>= ndistance 0)) (assoc! :x x :y y :distance ndistance)
    (not scontent) (assoc! :content (get-in arena [y x]))))

(defn summarize-regions
  [arena {rx :x ry :y :as reference} coloration summaries]
  (reduce (fn [summaries [x y]]
            (let [color (get-2d coloration x y)
                  ^long ndistance (distance x y rx ry)]
              (assoc! summaries color
                      (update-region-summary arena (nth summaries color) x y ndistance))))
          summaries
          (for [x (range max-x)
                y (range max-y)]
            [x y])))

(defn initialize-summaries
  [max-color]
  (let [init-coord (+ max-x max-y)]
    (transient
     (mapv #(transient {:num-points 0
                        :min-x init-coord
                        :max-x -1
                        :min-y init-coord
                        :max-y -1
                        :x init-coord
                        :y init-coord
                        :distance init-coord
                        :content nil
                        :color %})
           (range max-color)))))

(defn regions-map
  [arena reference]
  (let [[coloration max-color] (flood-fill-arena arena)
        summaries (initialize-summaries max-color)]
    (summarize-regions arena reference coloration summaries)
    (mapv persistent! (rest (persistent! summaries)))))

(defn select-target-region
  [summaries opponents]
  (let [spacies (filter #(= \. (:content %)) summaries)]
    (when-not (empty? spacies)
      (apply max-key
             (fn [{:keys [x y num-points min-x max-x min-y max-y] sdist :distance :as summary}]
               (let [width (inc (- max-x min-x))
                     height (inc (- max-y min-y))
                     aspect-ratio (/ (max width height) (min width height))
                     opponents-score (reduce (fn [r {ox :x oy :y}]
                                               (+ r (distance x y ox oy)))
                                             0
                                             opponents)]
                 (long (/ (* num-points num-points num-points (inc opponents-score))
                          (inc (* sdist sdist))
                          aspect-ratio))))
             spacies))))

(defn build-migration-plan
  [arena me opponents target-closest]
  (let [range-seed (+ 6 (int (Math/pow (:round me) 0.3333)))]
    (loop [tries 0 target0 target-closest]
      (cond
        (>= tries max-tries) [(assoc target-closest :phase ::migration)]
        target0 (let [round (:round me)
                      me0 (assoc me :x (:x target0) :y (:y target0))
                      box-plan0 (when target0
                                  (plan-box arena me0 opponents))
                      target (when box-plan0
                               (apply min-key (partial distance me) box-plan0))
                      box-plan (when target
                                 (let [[pre-t post-t] (split-with (partial not= target) box-plan0)]
                                   (concat (rest post-t) pre-t [target])))]
                  (if box-plan
                    (vec (concat [(assoc target :phase ::migration)] box-plan))
                    (recur (inc tries) (select-randomly #{\.} arena me opponents range-seed))))
        :else (recur (inc tries)
                     (select-randomly #{\.} arena me opponents range-seed))))))

(defn migration-plan
  [arena me opponents]
  (let [summaries (regions-map arena me)
        target-closest (select-target-region summaries opponents)]
    (build-migration-plan arena me opponents target-closest)))

(defn count-empty-spaces
  "Counts empty spaces on a path from [cx,cy] to [px,py], taking the most
  direct route between the two locations."
  [arena cx cy px py]
  (loop [spaces 0 cx cx cy cy]
    (if (and (= cx px) (= cy py))
      spaces
      (let [[nx ny] (cond
                      (= cx px) [cx (+ cy (Integer/signum (- py cy)))]
                      (= cy py) [(+ cx (Integer/signum (- px cx))) cy]
                      (= (rand-int 2) 0) [cx (+ cy (Integer/signum (- py cy)))]
                      :else [(+ cx (Integer/signum (- px cx))) cy])]
        (if (= \. (get-in arena [cy cx]))
          (recur (inc spaces) nx ny)
          (recur spaces nx ny))))))

(defn best-next-step
  "Selects the next step to take, optimizing for the largest number of
  empty spaces expected given two possible options for the next step."
  [arena opponents px py mx my]
  (let [cx1 mx
        cy1 (+ my (Integer/signum (- py my)))
        cx2 (+ mx (Integer/signum (- px mx)))
        cy2 my]
    (if (> (count-empty-spaces arena cx1 cy1 px py) (count-empty-spaces arena cx2 cy2 px py))
      {:x cx1 :y cy1 :phase ::migration}
      {:x cx2 :y cy2 :phase ::migration})))

(defmethod apply-plan ::migration
  [plan arena me opponents]
  (let [{px :x py :y} (first plan)
        {mx :x my :y} me]
    (if (or (= px mx) (= py my))
      plan
      (concat [(best-next-step arena opponents px py mx my)]
              plan))))

(defmethod terminate? ::migration
  [plan arena me opponents]
  (let [rest-plan (->> plan
                       (drop-while #(= (:phase %) ::migration))
                       (take-while #(= (:phase %) ::box)))]
    (and (seq rest-plan)
         (terminate? rest-plan arena (last rest-plan) opponents))))

;;; Main

(defn make-new-plan
  [arena me opponents]
  (or (quick-completion-plan arena me opponents)
      (plan-box arena me opponents)
      (migration-plan arena me opponents)))

(defn update-plan
  [plan arena me opponents]
  (cond
    (terminate? plan arena me opponents) (make-new-plan arena me opponents)
    (leg-completed? (first plan) arena me opponents) (if (seq (rest plan))
                                                       (apply-plan (rest plan) arena me opponents)
                                                       (make-new-plan arena me opponents))
    :else (apply-plan plan arena me opponents)))

(defn -main [& args]
  (let [opponent-count (read)]
    ;; opponentCount: Opponent count
    (loop [plan nil last-round 0 shifted? true]
      (let [{^long round :round :as me} (read-me)
            opponents (read-opponents opponent-count)
            arena (read-arena)
            new-plan (if (or (nil? plan) (< round last-round))
                       (make-new-plan arena me opponents)
                       (update-plan plan arena me opponents))
            _ (debug (:phase (first new-plan)))
            {:keys [x y]} (first new-plan)]
        (if (and (= round 100)
                 (not shifted?))
          (do
            (println "BACK" 24)
            (recur new-plan round true))
          (do
            (println x y)
            (recur new-plan round shifted?)))))))
