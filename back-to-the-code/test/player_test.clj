(ns player-test
  (:require [clojure.test :refer :all]
            [Player :as p]))

(def empty-arena
  (vec (repeat p/max-y (vec (repeat p/max-x \.)))))

(defn fill-arena [arena val & all-pos]
  (reduce (fn [arena pos]
            (assoc-in arena [(:y pos) (:x pos)] val))
          arena
          all-pos))

(deftest empty-arena-does-not-terminate
  (let [me {:x 10 :y 10}
        arena (fill-arena empty-arena \0 me)
        plan [{:x 10, :y 16, :phase ::p/box}
              {:x 16, :y 16, :phase ::p/box}
              {:x 16, :y 10, :phase ::p/box}
              {:x 10, :y 10, :phase ::p/box}]]
    (is (not (p/terminate? plan arena me [])))))

(deftest blocked-plan-terminates
  (let [me {:x 10 :y 10}
        arena (fill-arena empty-arena \0 me)
        plan [{:x 10, :y 16, :phase ::p/box}
              {:x 16, :y 16, :phase ::p/box}
              {:x 16, :y 10, :phase ::p/box}
              {:x 10, :y 10, :phase ::p/box}]]
    (testing "blockage in leg 1"
      (let [arena (fill-arena arena \1 {:x 10 :y 12})]
        (is (p/terminate? plan arena me []))))
    (testing "blockage in leg 2"
      (let [arena (fill-arena arena \1 {:x 12 :y 16})]
        (is (p/terminate? plan arena me []))))
    (testing "blockage in leg 3"
      (let [arena (fill-arena arena \1 {:x 16 :y 12})]
        (is (p/terminate? plan arena me []))))
    (testing "blockage in leg 4"
      (let [arena (fill-arena arena \1 {:x 12 :y 10})]
        (is (p/terminate? plan arena me []))))))

(deftest not-blocked-plan
  (let [me {:x 10 :y 10}
        arena (-> empty-arena
                  (fill-arena \0 me)
                  (fill-arena \1 {:x 10 :y 17}))
        plan [{:x 10, :y 16, :phase ::p/box}
              {:x 16, :y 16, :phase ::p/box}
              {:x 16, :y 10, :phase ::p/box}
              {:x 10, :y 10, :phase ::p/box}]]
    (is (not (p/terminate? plan arena me [])))))

(deftest selects-closest
  (let [me {:x 10 :y 10}
        arena (-> empty-arena
                  (fill-arena \0 me)
                  (fill-arena \1
                              {:x 9 :y 9}
                              {:x 9 :y 10}
                              {:x 9 :y 11}
                              {:x 10 :y 9}
                              {:x 10 :y 11}
                              {:x 11 :y 9}
                              {:x 11 :y 11}))
        closest (p/select-closest-empty-square arena me [])]
    (is (= (:x closest) 11))
    (is (= (:y closest) 10))))

(deftest selects-best-plan-destination
  (letfn [(corner-of-selection
            [me opponents & more-fills]
            (let [arena (apply fill-arena (fill-arena empty-arena \0 me)
                               \1 more-fills)
                  plan (p/plan-box arena (assoc me :round 0) opponents)]
              (select-keys (second plan) [:x :y])))]
    (testing "nothing in arena"
      (is (= (corner-of-selection {:x 20 :y 15} []) {:x 34 :y 0})))
    (testing "best corner selection has stuff"
      (is (= (corner-of-selection {:x 20 :y 15} [] {:x 1 :y 1}) {:x 34 :y 0})))
    (testing "selects towards opponent"
      (is (= (corner-of-selection {:x 30 :y 15} [{:x 1 :y 1}] {:x 1 :y 1})
             {:x 15 :y 7})))))

(deftest region-calculation
  (testing "emtpy arena is one region"
    (let [summaries (p/regions-map empty-arena {:x 1 :y 1})]
      (is (= (count summaries) 1))
      (is (= (:distance (first summaries)) 0)))))
